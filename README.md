# GoWeather
Retrieving weather data from the [OpenWeatherMap](https://openweathermap.org) current weather data API.

This is my first project using Go. You can use this application (with your own API key of course) to import weather data into your scripts! For example, you could make a custom module for a [polybar](https://github.com/jaagr/polybar).
